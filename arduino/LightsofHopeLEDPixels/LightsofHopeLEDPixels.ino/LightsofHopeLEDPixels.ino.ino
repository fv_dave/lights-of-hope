#include <MicroView.h>
#include "Adafruit_WS2801.h"
#include "SPI.h" // Comment out this line if using Trinket or Gemma
#ifdef __AVR_ATtiny85__
 #include <avr/power.h>
#endif


/*****************************************************************************
Example sketch for driving Adafruit WS2801 pixels!


  Designed specifically to work with the Adafruit RGB Pixels!
  12mm Bullet shape ----> https://www.adafruit.com/products/322
  12mm Flat shape   ----> https://www.adafruit.com/products/738
  36mm Square shape ----> https://www.adafruit.com/products/683

  These pixels use SPI to transmit the color data, and have built in
  high speed PWM drivers for 24 bit color per pixel
  2 pins are required to interface

  Adafruit invests time and resources providing this open source code, 
  please support Adafruit and open-source hardware by purchasing 
  products from Adafruit!

  Written by Limor Fried/Ladyada for Adafruit Industries.  
  BSD license, all text above must be included in any redistribution

*****************************************************************************/

// Choose which 2 pins you will use for output.
// Can be any valid output pins.
// The colors of the wires may be totally different so
// BE SURE TO CHECK YOUR PIXELS TO SEE WHICH WIRES TO USE!
uint8_t dataPin  = 2;    // Yellow wire on Adafruit Pixels
uint8_t clockPin = 3;    // Green wire on Adafruit Pixels

// Don't forget to connect the ground wire to Arduino ground,
// and the +5V wire to a +5V supply

// Set the first variable to the NUMBER of pixels. 25 = 25 pixels in a row
Adafruit_WS2801 strip = Adafruit_WS2801(50, dataPin, clockPin);

// Optional: leave off pin numbers to use hardware SPI
// (pinout is then specific to each board and can't be changed)
//Adafruit_WS2801 strip = Adafruit_WS2801(25);

// For 36mm LED pixels: these pixels internally represent color in a
// different format.  Either of the above constructors can accept an
// optional extra parameter: WS2801_RGB is 'conventional' RGB order
// WS2801_GRB is the GRB order required by the 36mm pixels.  Other
// than this parameter, your code does not need to do anything different;
// the library will handle the format change.  Examples:
//Adafruit_WS2801 strip = Adafruit_WS2801(25, dataPin, clockPin, WS2801_GRB);
//Adafruit_WS2801 strip = Adafruit_WS2801(25, WS2801_GRB);

void setup() {
#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000L)
  clock_prescale_set(clock_div_1); // Enable 16 MHz on Trinket
#endif
  uView.begin();          // Start MicroView
  uView.clear(PAGE);      // Clear Display
  uView.setCursor(0,0);
  uView.print("Ready...");
  uView.display();
  strip.begin();

  // Update LED contents, to start they are all 'off'
  strip.show();

  Serial.begin(9600);  
  //initialize strip - dim red sweep
  colorWipe(Color(1, 0, 0), 50);
  //initialize strip - dim green sweep
  colorWipe(Color(0, 1, 0), 50);
  //initialize strip - dim blue sweep
  colorWipe(Color(0, 0, 1), 50);
  //set strip to standby - all lights off
  colorWipe(Color(0, 0, 0), 50);
}


void loop() {

  int receivedLights [50] = {};
  int index;
  int lightRead;
    
  while (Serial.available() > 0) {
    lightRead = Serial.read();
    delay(5);
    if (lightRead == '\n'){
      delay(5);
      break;
    }
    else{
      receivedLights [index] = int(lightRead) - '0';
      index++;
      uView.setCursor(0,0);
      uView.print("Connected!");
      uView.setCursor(0,20);
      uView.print("Hello,");
      uView.setCursor(0,30);
      uView.print("Uganda!");
      uView.display();
    }
  }
  
  int i;
  int wait = 100;
  uint32_t c = Color(120, 120, 120);
  uint32_t d = Color(20, 20, 20);
  
  
  int helloLight[] = {
    0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49
    };
  int lightCount = 50;

  for (i=0; i < lightCount; i+=2) {

  if (receivedLights[i]==1){
    strip.setPixelColor(helloLight[i], c);
    strip.show();
    delay(wait);
    }
  }

  for (i=1; i < lightCount; i+=2) {

  if (receivedLights[i]==1){
    strip.setPixelColor(helloLight[i], d);
    strip.show();
    delay(wait);
    }
  }

  delay(600000);

  int cycleCount = 5;
  for (i=0; i<cycleCount; i++) {
    colorWipe(Color(40, 0, 0), 10);
    colorWipe(Color(0, 40, 0), 10);
    colorWipe(Color(0, 0, 40), 10);
  }

    colorWipe(Color(0, 0, 0), 10);
  
}

void colorWipe(uint32_t c, uint8_t wait) {
  int i;
  
  for (i=0; i < 50; i++) {
      strip.setPixelColor(i, c);
      strip.show();
      delay(wait);
  }
}

/* Helper functions */

// Create a 24 bit color value from R,G,B
uint32_t Color(byte r, byte g, byte b)
{
  uint32_t c;
  c = r;
  c <<= 8;
  c |= g;
  c <<= 8;
  c |= b;
  return c;
}

//Input a value 0 to 255 to get a color value.
//The colours are a transition r - g -b - back to r
uint32_t Wheel(byte WheelPos)
{
  if (WheelPos < 85) {
   return Color(WheelPos * 3, 255 - WheelPos * 3, 0);
  } else if (WheelPos < 170) {
   WheelPos -= 85;
   return Color(255 - WheelPos * 3, 0, WheelPos * 3);
  } else {
   WheelPos -= 170; 
   return Color(0, WheelPos * 3, 255 - WheelPos * 3);
  }
}
