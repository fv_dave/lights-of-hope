from pyramid.config import Configurator


def main(global_config, **settings):
    """ This function returns a Pyramid WSGI application.
    """
    config = Configurator(settings=settings)
    config.include('pyramid_chameleon')
    config.add_renderer('.html', 'pyramid_chameleon.zpt.renderer_factory')
    config.add_static_view('static', 'static', cache_max_age=3600)
    config.add_route('home', '/')
    config.add_route('id', '/{id:\d+}')
    config.add_route('lights', '/lights')
    config.add_route('catchall_static', '/*subpath')
    config.add_view('lights_of_hope.views.static_view', route_name='catchall_static')
    config.scan()
    return config.make_wsgi_app()
