import json
import os
import datetime
from pyramid.renderers import render_to_response
from pyramid.static import static_view
from pyramid.httpexceptions import HTTPFound
from pyramid.response import Response
from pyramid.view import view_config

names = [
    "Andrew",
    "PED Family",
    "Drew",
    "Katrin",
    "Larissa",
    "Versant Team",
    "Jason",
    "Christina",
    "Eli",
    "Boneo Canine Team",
    "Jason",
    "Martavis",
    "Ryan",
    "Arin",
    "John",
    "Merrit",
    "Hyperkinetic Studios",
    "Dena & Team",
    "Michael",
    "Alanna & Julietta",
    "Dan & Will",
    "Aviel",
    "Samantha",
    "Devon",
    "Emma",
    "Curt & Mason",
    "Claudia & Jane",
    "Diane",
    "Tony & John",
    "Angela",
    "Socorro",
    "Aaron",
    "Shannon",
    "Elliot",
    "Fred",
    "Drew",
    "Kendall & Elizabeth",
    "Steven",
    "Kelly",
    "Jeff",
    "Jennifer",
    "Jason",
    "OW! Entertainment",
    "Michael",
    "Fred",
    "Steven",
    "Kari",
    "Oz",
    "Rosella",
    "Joe"
]


path = os.path.dirname(os.path.realpath(__file__)) + "/static/lights.json"
f = open(path, 'r')
lights_list = json.load(f)
f.close()


def save_list():
    f = open(path, 'w')
    json.dump(lights_list, f)
    f.close()


@view_config(route_name='home', renderer='static/index.html')
def home(request):
    thanks = bool(request.cookies.get('thanks', False))
    response = render_to_response('static/index.html', {'thanks': thanks})
    response.delete_cookie('thanks')
    return response


@view_config(route_name='id', renderer='static/landing.html')
def id_lights(request):
    light_id = int(request.matchdict.get('id', -1))
    if request.method == 'GET':
        name = names[light_id] if light_id < len(names) else "Ron Swanson"
        return {"name": name}
    if 0 <= light_id < len(lights_list):
        dic = {'light': light_id, 'dt_visited': str(datetime.datetime.utcnow())}
        lights_list[light_id] = dic
        save_list()
    response = Response()
    response.set_cookie('thanks', value='True')
    return HTTPFound('/', headers=response.headers)


@view_config(route_name="lights")
def api_lights(request):
    s = ''
    for dic in lights_list:
        d = dic.get('dt_visited')
        v = '0' if d is None else '1'
        s += v
    return Response(body=s)

static_view = static_view('lights_of_hope:static', use_subpath=True)
