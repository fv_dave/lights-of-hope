#!/usr/bin/python
import sys
import json
import os

default_num_lights = 50


def reset_lights(num_lights):
    if type(num_lights) is not int:
        num_lights = default_num_lights
    light_list = [{'light': x, 'dt_visited': None} for x in range(num_lights)]
    path = os.path.abspath(os.path.dirname(os.path.realpath(__file__))) + '/../static/lights.json'
    print('writing to ' + path)
    f = open(path, 'w')
    json.dump(light_list, f)
    f.close()
    print('done.')


if __name__ == "__main__":
    nl = default_num_lights
    if len(sys.argv) > 1:
        try:
            nl = int(sys.argv[1])
        except Exception as err:
            print(str(err))
            print('Using default number of lights: %s' % nl)
    reset_lights(nl)
