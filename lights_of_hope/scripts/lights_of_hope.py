import urllib
import time
import serial


url = "http://hellouganda.friendlyvengeance.com/lights"
# url = "http://0.0.0.0:6543/lights"
cereal = serial.Serial('/dev/ttyACM0')


def main():
    try:
        while True:
            res = urllib.urlopen(url)
            lights = res.read() + "\n"
            cereal.write(lights)
            time.sleep(5)
    except KeyboardInterrupt:
        print('')
        exit()
    except Exception as err:
        print(str(err))
        exit()


if __name__ == '__main__':
    main()
