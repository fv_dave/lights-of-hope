$(document).ready(function(){
    var btnMenu = document.getElementById("btnMenu");
    var menu = $("#menu");
    var isMenuOpen = false;

    menu.removeClass("hide");
    menu.hide();
    var menuLinks = document.getElementsByClassName("index-menu-link");
    for (var i = 0; i < menuLinks.length; i++) {
        var item = menuLinks[i];
        item.onclick = handleClick.bind(this);
    }

    btnMenu.onclick = function() {
        isMenuOpen ? hideMenu() : showMenu();
    };

    function hideMenu() {
        menu.fadeOut();
        isMenuOpen = false;
    }

    function showMenu() {
        menu.fadeIn();
        isMenuOpen = true;
    }

    function handleClick(event) {
        hideMenu();
        var item = event.target;
        var href = item.getAttribute("data-href");
        var trgt = item.getAttribute("data-target");
        console.log("href: " + href + ";  target: " + trgt);
        if (trgt) {
            window.open(href, trgt);
        } else {
            $('html, body').animate({
                scrollTop: $(href).offset().top
            }, 500);
        }
    }
});